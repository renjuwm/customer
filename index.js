/****
 * Created by Renju on 2/17/2016.
 */
"use strict";
module.exports = {
    customer: require('./src/customer'),
    customerModel: require('./model/customer.model').model
};