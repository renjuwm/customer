/****
 * Created by Renju on 2/11/2016.
 * @TODO : Validation before update and save
 */
'use strict';

let mongoose = require('mongoose');

/*

 */

let customerSchema = new mongoose.Schema({
        first_name: {
            type: String,
            required: true,
            trim: true
        },
        last_name: {
            type: String,
            required: true,
            trim: true
        },
        find_name: {
            type: String,
            unique: true,
            required: true
        },
        company: {
            id: {
                type: mongoose.Schema.Types.ObjectId
            },
            company_name: {
                type: String,
                required: true,
                trim: true
            }
        },
        title: {
            type: String,
            trim: true
        },
        emails: [{
            type: {
                type: String,
                required: true
            },
            value: {
                type: String,
                required: true
            },
            primary: Boolean
        }
        ],
        phone_numbers: [{
            type: {
                type: String
            },
            value: {
                type: String
            },
            primary: Boolean
        }
        ],
        addresses: [{
            type: {
                type: String
            },
            street: {
                type: String
            },
            city: {
                type: String
            },
            state: {
                type: String
            },
            country: {
                type: String
            },
            pincode: {
                type: Number
            }
        }
        ]
    },
    {
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at'
        }
    }
);


/*
 Function will be called before saving model to database
 */
customerSchema.pre('save', function (next) {
    var doc = this;
    next();
});

module.exports = {
    schema: customerSchema,
    model: mongoose.model('Customer', customerSchema)
};