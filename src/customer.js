/**
 * Created by theverma on 2/21/2016.
 * @TODO : Add Pre Validation While Creating and Updating Customer
 */
"use strict";
let Customer = require('./../model/customer.model').model;
let debug = require('debug')('ridgeback.customer');

let createCustomer = (newCustomer, next) => {
    debug("Customer Create : Called ");
    return newCustomer.save().then(
        (customer) => {
            debug("Customer Create : Saved => " + customer);
            return next(undefined, customer);
        }
    ).catch(error => {
        debug("Customer Create : Exception => " + error);
        next(error, null)
    });

};

let getCustomerById = (customerId, next) => {
    debug("Customer getCustomerById : Called ");
    return Customer.findById(customerId)
        .then((customer) => {
            debug("Customer getCustomerById : found issue => " + customer);
            return next(undefined, customer);
        })
        .catch(error => {
            debug("Customer getCustomerById : Exception => " + error);
            next(error, null)
        });

};

let updateCustomer = (updateCustomer, next) => {
    debug("Customer updateCustomer : Called ");
    return Customer.findById(updateCustomer._id)
        .then((dbCustomer) => {
            debug("Customer updateCustomer : found issue => " + dbCustomer);
            dbCustomer.first_name = updateCustomer.first_name || dbCustomer.first_name;
            dbCustomer.last_name = updateCustomer.last_name || dbCustomer.last_name;
            dbCustomer.find_name = updateCustomer.find_name || dbCustomer.find_name;
            dbCustomer.company.id = updateCustomer.company.id || dbCustomer.company.id;
            dbCustomer.company.company_name = updateCustomer.company.company_name || dbCustomer.company.company_name;
            dbCustomer.title = updateCustomer.title || dbCustomer.title;
            return dbCustomer.save()
                .then((customer) => {
                    debug("Customer updateCustomer : updated customer => " + customer);
                    return next(undefined, customer);
                })
                .catch(error => {
                    debug("Customer Exception save : updated customer => " + error);
                    next(error, null)
                });
        })
        .catch(error => {
            debug("Customer Exception findBy: updated customer => " + error);
            next(error, null)
        });
};

module.exports = {
    create: createCustomer,
    getById: getCustomerById,
    update: updateCustomer
};