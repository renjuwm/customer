// Tests for customer features
// @Author : Renju
// When you add new test cases add it in the end
// @TODO : Test Cases for Validation

"use strict";
var debug = require('debug')('customer.test');
var assert = require('chai').assert;
var test_helper = require('./fixtures/test-helper');
var faker = require('faker');
var mongoose = require('mongoose');
var Customer = require('./../model/customer.model').model;
var customerLib = require('./../index').customer;

before((done) => {
    test_helper.createDB(() => {
        "use strict";
        done();
        debug("Before : DB Created");
    });
});

after(() => {
    test_helper.dropDB();
    debug("After : DB Dropped");
});

//test 1 is equal to 1
describe("Base Test", () => {
    it("1 should be equal to 1", () => {
        assert.strictEqual(1, 1, "These values are strict equal");
    });
});


//test mongoose db
describe("Database", () => {
    "use strict";

    before((done) => {
        test_helper.resetDB(() => {
            done();
            debug("Database Test => Before  : DB Reset");
        });
    });

    it("Mongoose lib should be mocked", (done) => {
        assert.strictEqual(test_helper.isMocked(), true, "Expecting mongoose is mocked");
        done();
    });

    it("Mongoose DB should be ready", () => {
        assert.strictEqual(test_helper.mongooseReadyState(), 1, "ReadyState should be 1");
    });

});

describe("Customer Module", () => {
    let new_customer;


    before((done) => {
        test_helper.resetDB(() => {
            debug('DB Reset Completed');
            new_customer = new Customer({
                first_name: faker.name.firstName(),
                last_name: faker.name.lastName(),
                find_name: faker.name.findName(),
                company: {
                    id: mongoose.Types.ObjectId(),
                    company_name: faker.company.companyName()
                },
                title: "CEO",
                emails: [{
                    type: "work",
                    value: faker.internet.email,
                    primary: true
                }],
                phone_numbers: [{
                    type: "work",
                    value: faker.phone.phoneNumber(),
                    primary: true
                }],
                addresses: [{
                    type: "work",
                    street: faker.address.streetName(),
                    city: faker.address.city,
                    state: faker.address.state,
                    country: faker.address.country,
                    pincode: faker.address.zipcode
                }]
            });
            done();
        });

    });

    it("should create the customer", (done) => {
            try {
                customerLib.create(new_customer, (error, customer) => {
                    assert.isUndefined(error, "error should be undefined " + JSON.stringify(error));
                    assert.isDefined(customer._id, "Customer id is required");
                    new_customer._id = customer._id;
                    assert.strictEqual(customer.first_name, new_customer.first_name, "first name should be equal");
                    assert.strictEqual(customer.last_name, new_customer.last_name, "last_name should be equal");
                    assert.strictEqual(customer.find_name, new_customer.find_name, "find_name should be equal");
                    assert.strictEqual(customer.company.id, new_customer.company.id, "company id should be equal");
                    assert.strictEqual(customer.company.company_name, new_customer.company.company_name, "company_name should be equal");
                    assert.strictEqual(customer.title, new_customer.title, "title should be equal");
                    assert.strictEqual(customer.emails[0].type, new_customer.emails[0].type, "emails.type should be equal");
                    assert.strictEqual(customer.emails[0].value, new_customer.emails[0].value, "emails.value should be equal");
                    assert.strictEqual(customer.emails[0].primary, new_customer.emails[0].primary, "emails.primary should be equal");
                    assert.strictEqual(customer.phone_numbers[0].type, new_customer.phone_numbers[0].type, "phone_numbers.type should be equal");
                    assert.strictEqual(customer.phone_numbers[0].value, new_customer.phone_numbers[0].value, "phone_numbers.value should be equal");
                    assert.strictEqual(customer.phone_numbers[0].primary, new_customer.phone_numbers[0].primary, "phone_numbers.primary should be equal");
                    assert.strictEqual(customer.addresses[0].type, new_customer.addresses[0].type, "addresses.type should be equal");
                    assert.strictEqual(customer.addresses[0].street, new_customer.addresses[0].street, "addresses.street should be equal");
                    assert.strictEqual(customer.addresses[0].city, new_customer.addresses[0].city, "addresses.city should be equal");
                    assert.strictEqual(customer.addresses[0].state, new_customer.addresses[0].state, "addresses.state should be equal");
                    assert.strictEqual(customer.addresses[0].country, new_customer.addresses[0].country, "addresses.country should be equal");
                    assert.strictEqual(customer.addresses[0].pincode, new_customer.addresses[0].pincode, "addresses.pincode should be equal");
                    done();
                });
            }
            catch (exp) {
                done(exp);
            }
        }
    );
    it("should read the customer", (done) => {
        try {
            customerLib.getById(new_customer._id, (error, customer) => {
                assert.isUndefined(error, "error should be undefined ");
                assert.isDefined(customer._id, "Customer id is required");
                assert.strictEqual(customer.first_name, new_customer.first_name, "first name should be equal");
                assert.strictEqual(customer.last_name, new_customer.last_name, "last_name should be equal");
                assert.strictEqual(customer.find_name, new_customer.find_name, "find_name should be equal");
                assert.strictEqual(customer.company.id.toString(), new_customer.company.id.toString(), "company id should be equal");
                assert.strictEqual(customer.company.company_name, new_customer.company.company_name, "company_name should be equal");
                assert.strictEqual(customer.title, new_customer.title, "title should be equal");
                assert.strictEqual(customer.emails[0].type, new_customer.emails[0].type, "emails.type should be equal");
                assert.strictEqual(customer.emails[0].value, new_customer.emails[0].value, "emails.value should be equal");
                assert.strictEqual(customer.emails[0].primary, new_customer.emails[0].primary, "emails.primary should be equal");
                assert.strictEqual(customer.phone_numbers[0].type, new_customer.phone_numbers[0].type, "phone_numbers.type should be equal");
                assert.strictEqual(customer.phone_numbers[0].value, new_customer.phone_numbers[0].value, "phone_numbers.value should be equal");
                assert.strictEqual(customer.phone_numbers[0].primary, new_customer.phone_numbers[0].primary, "phone_numbers.primary should be equal");
                assert.strictEqual(customer.addresses[0].type, new_customer.addresses[0].type, "addresses.type should be equal");
                assert.strictEqual(customer.addresses[0].street, new_customer.addresses[0].street, "addresses.street should be equal");
                assert.strictEqual(customer.addresses[0].city, new_customer.addresses[0].city, "addresses.city should be equal");
                assert.strictEqual(customer.addresses[0].state, new_customer.addresses[0].state, "addresses.state should be equal");
                assert.strictEqual(customer.addresses[0].country, new_customer.addresses[0].country, "addresses.country should be equal");
                assert.strictEqual(customer.addresses[0].pincode, new_customer.addresses[0].pincode, "addresses.pincode should be equal");
                done();
            });
        } catch (exp) {
            done(exp);
        }
    });

    let modify_customer = new Customer();

    it("should update the customer", (done) => {
        modify_customer._id = new_customer._id;
        modify_customer.first_name = new_customer.first_name + " modified";
        modify_customer.last_name = new_customer.last_name + " modified";
        modify_customer.find_name = new_customer.find_name + " modified";
        modify_customer.company.id = mongoose.Types.ObjectId();
        modify_customer.company.company_name = new_customer.company.company_name + " modified";
        modify_customer.title = new_customer.title + " modified";
        customerLib.update(modify_customer, (error, customer) => {
            assert.isUndefined(error, "error should be undefined ");
            assert.isDefined(customer._id, "Customer id is required");
            assert.strictEqual(customer.first_name, modify_customer.first_name, "first name should be equal");
            assert.strictEqual(customer.last_name, modify_customer.last_name, "last_name should be equal");
            assert.strictEqual(customer.find_name, modify_customer.find_name, "find_name should be equal");
            assert.strictEqual(customer.company.id.toString(), modify_customer.company.id.toString(), "company id should be equal");
            assert.strictEqual(customer.company.company_name, modify_customer.company.company_name, "company_name should be equal");
            assert.strictEqual(customer.title, modify_customer.title, "title should be equal");
            done();
        });
    });


    it("should add new email fpr customer");
    it("should remove email for customer");
    it("should update exiting work email for customer");
    it("should add new phone_number fpr customer");
    it("should remove phone_number for customer");
    it("should update exiting work phone_number for customer");
    it("should add new address for customer");
    it("should remove address for customer");
    it("should update exiting work address for customer");
    it("should validate while adding new customer");
    it("should validate while updating existing customer");

});









