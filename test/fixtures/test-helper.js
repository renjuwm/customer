/****
 * Created by renju on 2/9/2016.
 */
var mongoose = require('mongoose');
var mockgoose = require('mockgoose');

mockgoose(mongoose);

/*
 Create and connects a test database in memory
 */
module.exports.createDB = (cb) => {
    "use strict";
    mongoose.connect('mongodb://localhost/test', cb);
};

/*
 Disconnect and destroy in-memory database
 */
module.exports.dropDB = () => {
    "use strict";
    mongoose.disconnect();
};

/*
 Get mongoose mocked status
 */
module.exports.isMocked = () => {
    "use strict";
    return mongoose.isMocked;
};

/*
 Get Mongoose Connection state
 */
module.exports.mongooseReadyState = () => {
    "use strict";
    return mongoose.connection.readyState;
};

/*
 Reset the DB
 */
module.exports.resetDB = (cb) => {
    "use strict";
    mockgoose.reset(cb);
};

